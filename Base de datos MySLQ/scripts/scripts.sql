CREATE DATABASE  IF NOT EXISTS `pruebas` /*!40100 DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci */ /*!80016 DEFAULT ENCRYPTION='N' */;
USE `pruebas`;
-- MySQL dump 10.13  Distrib 8.0.28, for Win64 (x86_64)
--
-- Host: localhost    Database: pruebas
-- ------------------------------------------------------
-- Server version	8.0.28

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!50503 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `areas`
--

DROP TABLE IF EXISTS `areas`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `areas` (
  `id_area` int NOT NULL AUTO_INCREMENT,
  `nombre` varchar(100) NOT NULL,
  PRIMARY KEY (`id_area`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `areas`
--

LOCK TABLES `areas` WRITE;
/*!40000 ALTER TABLE `areas` DISABLE KEYS */;
INSERT INTO `areas` VALUES (1,'Administración'),(2,'Financiera'),(3,'Compras'),(4,'Infraestructura'),(5,'Operación'),(6,'Talento Humano'),(7,'Servicios Varios');
/*!40000 ALTER TABLE `areas` ENABLE KEYS */;
UNLOCK TABLES;



--
-- Table structure for table `paises`
--

DROP TABLE IF EXISTS `paises`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `paises` (
  `id_pais` int NOT NULL AUTO_INCREMENT,
  `nombre` varchar(100) NOT NULL,
  `dominio` varchar(10) NOT NULL,
  PRIMARY KEY (`id_pais`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `paises`
--

LOCK TABLES `paises` WRITE;
/*!40000 ALTER TABLE `paises` DISABLE KEYS */;
INSERT INTO `paises` VALUES (1,'Colombia','co'),(2,'USA','us');
/*!40000 ALTER TABLE `paises` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tipos_documentos`
--

DROP TABLE IF EXISTS `tipos_documentos`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `tipos_documentos` (
  `id_tipo_documento` int NOT NULL AUTO_INCREMENT,
  `nombre` varchar(100) NOT NULL,
  `abreviacion` varchar(10) NOT NULL,
  PRIMARY KEY (`id_tipo_documento`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tipos_documentos`
--

LOCK TABLES `tipos_documentos` WRITE;
/*!40000 ALTER TABLE `tipos_documentos` DISABLE KEYS */;
INSERT INTO `tipos_documentos` VALUES (1,'Cedula','CC'),(2,'Pasaporte','PA');
/*!40000 ALTER TABLE `tipos_documentos` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2022-02-06 21:18:18


--
-- Table structure for table `empleados`
--

DROP TABLE IF EXISTS `empleados`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `empleados` (
  `id_empleado` int NOT NULL AUTO_INCREMENT,
  `primer_nombre` varchar(20) NOT NULL,
  `primer_apellido` varchar(20) NOT NULL,
  `segundo_apellido` varchar(20) NOT NULL,
  `otros_nombres` varchar(50) DEFAULT NULL,
  `id_tipo_identificacion` int DEFAULT NULL,
  `identificacion` varchar(20) DEFAULT NULL,
  `id_pais` int NOT NULL,
  `correo_electronico` varchar(300) NOT NULL,
  `fecha_ingreso` datetime NOT NULL,
  `id_area` int NOT NULL,
  `estado` int NOT NULL DEFAULT '1',
  `fecha_registro` datetime NOT NULL,
  `fecha_edicion` datetime NOT NULL,
  PRIMARY KEY (`id_empleado`),
  UNIQUE KEY `correo_electronico_UNIQUE` (`correo_electronico`),
  KEY `fk_pais_idx` (`id_pais`),
  KEY `fk_area_idx` (`id_area`),
  KEY `fk_tipo_identificacion_idx` (`id_tipo_identificacion`),
  CONSTRAINT `fk_area` FOREIGN KEY (`id_area`) REFERENCES `areas` (`id_area`) ON DELETE RESTRICT ON UPDATE CASCADE,
  CONSTRAINT `fk_pais` FOREIGN KEY (`id_pais`) REFERENCES `paises` (`id_pais`) ON DELETE RESTRICT ON UPDATE CASCADE,
  CONSTRAINT `fk_tipo_identificacion` FOREIGN KEY (`id_tipo_identificacion`) REFERENCES `tipos_documentos` (`id_tipo_documento`) ON DELETE RESTRICT ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=31 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `empleados`
--

LOCK TABLES `empleados` WRITE;
/*!40000 ALTER TABLE `empleados` DISABLE KEYS */;
INSERT INTO `empleados` VALUES (19,'Carlos','Sandoval','Lozano','Guillermo Juan',1,'US1121913599',2,'carlossandoval@cidenet.com.us','2022-02-02 00:00:00',5,1,'2022-02-06 20:16:47','2022-02-06 20:38:23'),(20,'Edwin','Sandoval','Lozano','Juan',2,'US1121913599',1,'edwinsandoval@cidenet.com.co','2022-02-03 00:00:00',2,1,'2022-02-06 20:18:49','2022-02-06 20:38:50'),(21,'Harold','Jara','Granados','',1,'44455666',2,'haroldjara@cidenet.com.us','2022-02-04 00:00:00',6,1,'2022-02-06 20:33:27','2022-02-06 20:33:27'),(22,'CAROLINA','HERNANDEZ','MONTERROZA','',1,'4455667788',1,'carolinahernandez@cidenet.com.co','2022-02-04 00:00:00',5,1,'2022-02-06 20:41:26','2022-02-06 20:41:26'),(23,'ENRIQUE','RODRIGUEZ','GOMEZ','CAMILO',2,'US11556699',2,'enriquerodriguez@cidenet.com.us','2022-02-01 00:00:00',3,1,'2022-02-06 20:42:14','2022-02-06 20:42:14'),(25,'Juan','apellido','apellido','',2,'7888055a577',2,'juanapellido@cidenet.com.us','2022-02-02 00:00:00',3,1,'2022-02-06 21:08:36','2022-02-06 21:08:36'),(26,'RAFAEL','PARRA','BORDA','ANDRES',1,'1121913599',2,'rafaelparra@cidenet.com.us','2022-02-03 00:00:00',6,1,'2022-02-06 21:09:33','2022-02-06 21:09:33'),(27,'YIRIAM','SABOGAL','SALAZAR','',1,'88996633',1,'yiriamsabogal@cidenet.com.co','2022-02-05 00:00:00',6,1,'2022-02-06 21:11:48','2022-02-06 21:11:48'),(28,'JUAN','BOTERO','REY','CAMILO',1,'US1121918899',2,'juanbotero@cidenet.com.us','2022-02-03 00:00:00',6,1,'2022-02-06 21:12:29','2022-02-06 21:12:29'),(29,'JUAN','CORTES','MEJIA','',2,'US1121913588',2,'juancortes@cidenet.com.us','2022-02-02 00:00:00',6,1,'2022-02-06 21:13:10','2022-02-06 21:13:10'),(30,'MARÍA','GUACAS','BUSTOS','',2,'US1125566',2,'maríaguacas@cidenet.com.us','2022-02-04 00:00:00',4,1,'2022-02-06 21:13:49','2022-02-06 21:13:49');
/*!40000 ALTER TABLE `empleados` ENABLE KEYS */;
UNLOCK TABLES;