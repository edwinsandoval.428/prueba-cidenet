import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';

import { HttpClientModule } from '@angular/common/http';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { NgxPaginationModule } from 'ngx-pagination'; 


import { EmpleadoIndexComponent } from './empleado-index/empleado-index.component';
import { NoFoundComponent } from './no-found/no-found.component';
import { CrearEmpleadoComponent } from './crear-empleado/crear-empleado.component';
import { EditarEmpleadoComponent } from './editar-empleado/editar-empleado.component';
import { FiltroEmpleadoPipe } from './pipes/filtro-empleado.pipe';




@NgModule({
  declarations: [
    AppComponent,
    EmpleadoIndexComponent,
    NoFoundComponent,
    CrearEmpleadoComponent,
    EditarEmpleadoComponent,
    FiltroEmpleadoPipe
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule,
    FormsModule,
    ReactiveFormsModule,
    NgxPaginationModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
