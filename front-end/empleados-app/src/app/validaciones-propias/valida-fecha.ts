import { AbstractControl } from "@angular/forms"; 
import { DatePipe } from "@angular/common";

export class ValidacionesEmpleado{


    toDay:any;
  
    static datePipeStatic:DatePipe;

    constructor(public datePipe:DatePipe){
        this.toDay = this.datePipe.transform(new Date(), 'yyyy-MM-dd');
    }


    getDateToDay(date:any) {
        this.toDay = this.datePipe.transform(date, 'yyyy-MM-dd');
     }

    static fechaMenorAHoy(control:AbstractControl){
        const value = control.value;


        var toDayFull = new Date();
        var toDay = new Date(toDayFull.toISOString().substring(0,10));
        
        var dateValue = new Date(value);


        if(toDay <= dateValue){
            return {fechaMenorAHoy:true}        
        }else{
            return null;
        }
    }

}