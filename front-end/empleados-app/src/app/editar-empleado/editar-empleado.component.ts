import { Component, OnInit } from '@angular/core';
import { ConsumoApiService } from '../services/consumo-api.service';
import { Router, ActivatedRoute } from '@angular/router';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { ValidacionesEmpleado } from '../validaciones-propias/valida-fecha';

@Component({
  selector: 'app-editar-empleado',
  templateUrl: './editar-empleado.component.html',
  styleUrls: ['./editar-empleado.component.css']
})
export class EditarEmpleadoComponent implements OnInit {

  formularioEmpleado:FormGroup; 
  id_empleado:any;
  paises:any;
  areas:any;
  tiposDocumentos:any;

  respuestaAPI:any;
  errores:any;

  constructor(
    public formulario:FormBuilder,
    private activeRoute:ActivatedRoute,
    private consumoApiService: ConsumoApiService,
    private router:Router
  ) { 
    this.id_empleado=this.activeRoute.snapshot.paramMap.get('id')
    console.log(this.id_empleado);
    
    this.consumoApiService.getEmpleado(this.id_empleado).subscribe(resp => {
     
      console.log(resp);
      this.formularioEmpleado.setValue({
        primer_nombre:resp[0]['primer_nombre'],
        otros_nombres:resp[0]['otros_nombres'],
        primer_apellido:resp[0]['primer_apellido'],
        segundo_apellido:resp[0]['segundo_apellido'],
        id_tipo_identificacion:resp[0]['id_tipo_identificacion'],
        identificacion:resp[0]['identificacion'],
        id_pais:resp[0]['id_pais'],        
        fecha_ingreso:resp[0]['fecha_ingreso'].substring(0,10),
        id_area:resp[0]['id_area']
      });
    },
    error => { console.error(error) }    
    );

    this.formularioEmpleado=this.formulario.group({
      primer_nombre:['', [Validators.required, Validators.pattern(/^[a-zA-Z]+$/), Validators.maxLength(20)]],
      otros_nombres:['',[Validators.required, Validators.pattern(/^[a-zA-Z ]+$/), Validators.maxLength(50)]],
      primer_apellido:['',[Validators.required, Validators.pattern(/^[a-zA-Z]+$/), Validators.maxLength(20)]],
      segundo_apellido:['',[Validators.required, Validators.pattern(/^[a-zA-Z]+$/), Validators.maxLength(20)]],
      id_pais:['',Validators.required],
      id_tipo_identificacion:['',Validators.required],
      identificacion:['',[Validators.required, Validators.pattern(/^[a-zA-Z0-9-]+$/), Validators.maxLength(20)]],
      fecha_ingreso:['',[Validators.required,ValidacionesEmpleado.fechaMenorAHoy]],
      id_area:['',Validators.required]
    });

    this.consumoApiService.getPaises().subscribe(resp => {     
      this.paises = resp;
      console.log(resp);
    },
    error => { console.error(error) }    
    );

    this.consumoApiService.getTiposDocumentos().subscribe(resp => {     
      this.tiposDocumentos = resp;
      console.log(resp);
    },
    error => { console.error(error) }    
    );

    this.consumoApiService.getAreas().subscribe(resp => {     
      this.areas = resp;
      console.log(resp);
    },
    error => { console.error(error) }    
    );

  }
  ngOnInit(): void {
    
  }

  actualizarEmpleado():any{
    console.log(this.id_empleado);
    console.log(this.formularioEmpleado.value);

    if(this.formularioEmpleado.valid){
      this.consumoApiService.actualizarEmpleado(this.id_empleado,this.formularioEmpleado.value).subscribe(resp => {     
        console.log("respuesta");
        console.log(resp);
        this.respuestaAPI = resp;
        if (typeof this.respuestaAPI.error != 'undefined' && this.respuestaAPI.error) {
          this.errores = this.respuestaAPI.error;
        }
        else{
          this.router.navigateByUrl("/");
        }     
      },
      error => { console.error(error) }    
      );
    }else {
      console.log("No enviar")
    }
    
  }

}
