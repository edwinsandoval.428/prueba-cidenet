import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { EmpleadoIndexComponent } from './empleado-index/empleado-index.component';
import { CrearEmpleadoComponent } from './crear-empleado/crear-empleado.component';
import { EditarEmpleadoComponent } from './editar-empleado/editar-empleado.component';
import { NoFoundComponent } from './no-found/no-found.component';

const routes: Routes = [
  {
    path:'',
    redirectTo:'/empleado-index',
    pathMatch:'full'
  },
  {
    path: 'empleado-index',
    component:EmpleadoIndexComponent
  },
  {
    path: 'crear-empleado',
    component:CrearEmpleadoComponent
  },
  {
    path: 'editar-empleado/:id',
    component:EditarEmpleadoComponent
  },
  {
    path: '**',
    component:NoFoundComponent
  },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
