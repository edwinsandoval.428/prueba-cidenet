import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { HttpClient } from '@angular/common/http';
import { Empleado } from '../models/Empleado.model';
import { RespuestaAPI } from '../models/respuestaAPI.model';

@Injectable({
  providedIn: 'root'
})
export class ConsumoApiService {

  constructor(
    private httpClient: HttpClient
  ) { }

  public getAllEmpleados(): Observable<any>{    
    console.log("empleados obtenidos");
    return this.httpClient.get('http://127.0.0.1:5000/api/getEmpleados');
  }

  public eliminarEmpleado(id_empleado:any):Observable<any>{
    console.log("Eliminar empleado");
     return this.httpClient.get('http://127.0.0.1:5000/api/eliminarEmpleado/'+id_empleado);
  }
  public crearEmpleado(empleado:Empleado): Observable<any>{     
    console.log("crear usuario");
    return this.httpClient.post('http://127.0.0.1:5000/api/crearEmpleado',empleado);
  }


  public getPaises(): Observable<any>{    
   console.log("paises obtenidos");
   return this.httpClient.get('http://127.0.0.1:5000/api/getPaises');
  }

  public getTiposDocumentos(): Observable<any>{    
   console.log("Tipos de documentos obtenidos");
   return this.httpClient.get('http://127.0.0.1:5000/api/getTiposDocumentos');
 }

 public getAreas(): Observable<any>{    
   console.log("areas obtenidos");
   return this.httpClient.get('http://127.0.0.1:5000/api/getAreas');
 }

 public getEmpleado(id_empleado:any): Observable<any>{
    
  console.log("empleado obtenido");
  return this.httpClient.get('http://127.0.0.1:5000/api/getEmpleado/'+id_empleado);
}


public actualizarEmpleado(id_empleado:any,empleado:Empleado): Observable<any>{
  
  console.log("empleado obtenido");
  return this.httpClient.post('http://127.0.0.1:5000/api/actualizarEmpleado/'+id_empleado,empleado);
}

}
