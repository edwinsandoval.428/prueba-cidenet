export interface Empleado {
    id_empleado: number;
    primer_nombre: string;
    otros_nombres: string;    
    primer_apellido:string;
    segundo_apellido:string;
    id_pais:number;
    pais:string;
    id_tipo_identificacion:number;
    tipo_identificacion:string;
    identificacion:string;
    correo_electronico:string;
    fecha_pingreso:string;
    id_area:number;
    area:string;
    estado:number;
    fecha_registro:string;
    fecha_edicion:string;  
  }