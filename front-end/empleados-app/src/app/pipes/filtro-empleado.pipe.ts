import { Pipe, PipeTransform } from '@angular/core';
import { Empleado } from '../models/Empleado.model';
import { EmpleadoIndexComponent } from '../empleado-index/empleado-index.component';

@Pipe({
  name: 'filtroEmpleado'
})
export class FiltroEmpleadoPipe implements PipeTransform {

  constructor(
    private empleadoIndexComponent: EmpleadoIndexComponent
  ) {    

  }


  validarCampos(empleado:Empleado, search:string) {


    const nombreCompleto = empleado.primer_nombre + " " + empleado.otros_nombres + " " + empleado.primer_apellido + " " + empleado.segundo_apellido;
    const nombreApellidos = empleado.primer_nombre + " " + empleado.primer_apellido + " " + empleado.segundo_apellido;
    const apellidosCompleto = empleado.primer_apellido + " " + empleado.segundo_apellido;
    if(nombreCompleto.toLowerCase().includes(search.toLowerCase())) return true;
    if(nombreApellidos.toLowerCase().includes(search.toLowerCase())) return true;
    if(apellidosCompleto.toLowerCase().includes(search.toLowerCase())) return true;
    if(empleado.tipo_identificacion.toLowerCase().includes(search.toLowerCase())) return true;
    if(empleado.identificacion.toLowerCase().includes(search.toLowerCase())) return true;
    if(empleado.pais.toLowerCase().includes(search.toLowerCase())) return true;
    if(empleado.correo_electronico.toLowerCase().includes(search.toLowerCase())) return true;
    

    
    return false;
  }
  transform(empleados: Empleado[], search:string): Empleado[] {


    const filtroEmpleados = empleados.filter(empleado => this.validarCampos(empleado,search));

    this.empleadoIndexComponent.totalEmpleados = filtroEmpleados.length;

    return filtroEmpleados;
  }

}
