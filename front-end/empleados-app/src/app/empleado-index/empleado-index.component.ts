import { Component, OnInit } from '@angular/core';
import { ConsumoApiService } from '../services/consumo-api.service';
import { Empleado } from '../models/Empleado.model';

@Component({
  selector: 'app-empleado-index',
  templateUrl: './empleado-index.component.html',
  styleUrls: ['./empleado-index.component.css']
})
export class EmpleadoIndexComponent implements OnInit {

  mostrarUsuarios: any;

  tipovar: any; 
  mostrar_empleados: Empleado[] = [];

  totalEmpleados:any;
  page:number = 1;
  search:string = "";

  constructor(
    private consumoApiService: ConsumoApiService
  ) {    

  }

  ngOnInit(): void {
    this.consumoApiService.getAllEmpleados().subscribe(resp => {     
      this.mostrar_empleados = resp;
      this.totalEmpleados = resp.length;
    },
    error => { console.error(error) }    
    );
  }

  eliminarEmpleado(id_empleado:any){
    console.log(id_empleado);

    if(window.confirm("Está seguro de que desea eliminar el empleado?")){
      this.consumoApiService.eliminarEmpleado(id_empleado).subscribe();
    }
  }

  buscarEmpleado(search:string){
    console.log(search);
    this.search = search
    
  }

}
