import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder,  FormControl, Validators } from '@angular/forms';
import { ConsumoApiService } from '../services/consumo-api.service';
import { Router,RouterLink } from '@angular/router';
import { ValidacionesEmpleado } from '../validaciones-propias/valida-fecha';
import { RespuestaAPI } from '../models/respuestaAPI.model';

@Component({
  selector: 'app-crear-empleado',
  templateUrl: './crear-empleado.component.html',
  styleUrls: ['./crear-empleado.component.css']
})
export class CrearEmpleadoComponent implements OnInit {

  formularioEmpleado:FormGroup; 

  paises:any;
  areas:any;
  tiposDocumentos:any;

  respuestaAPI:any;

  errores:any;


  constructor(
    public formulario:FormBuilder,
    private consumoApiService:ConsumoApiService,
    private router:Router 
  ) { 

    this.formularioEmpleado=this.formulario.group({
      primer_nombre:['', [Validators.required, Validators.pattern(/^[a-zA-Z]+$/), Validators.maxLength(20)]],
      otros_nombres:['',[ Validators.pattern(/^[a-zA-Z ]+$/), Validators.maxLength(50)]],
      primer_apellido:['',[Validators.required, Validators.pattern(/^[a-zA-Z]+$/), Validators.maxLength(20)]],
      segundo_apellido:['',[Validators.required, Validators.pattern(/^[a-zA-Z]+$/), Validators.maxLength(20)]],
      id_pais:['',Validators.required],
      id_tipo_identificacion:['',Validators.required],
      identificacion:['',[Validators.required, Validators.pattern(/^[a-zA-Z0-9-]+$/), Validators.maxLength(20)]],
      fecha_ingreso:['',[Validators.required,ValidacionesEmpleado.fechaMenorAHoy]],
      id_area:['',Validators.required]
    });

    this.consumoApiService.getPaises().subscribe(resp => {     
      this.paises = resp;
      console.log(resp);
    },
    error => { console.error(error) }    
    );

    this.consumoApiService.getTiposDocumentos().subscribe(resp => {     
      this.tiposDocumentos = resp;
      console.log(resp);
    },
    error => { console.error(error) }    
    );

    this.consumoApiService.getAreas().subscribe(resp => {     
      this.areas = resp;
      console.log(resp);
    },
    error => { console.error(error) }    
    );


  }

  ngOnInit(): void {
  }

  enviarDatos():any{  

    this.consumoApiService.crearEmpleado(this.formularioEmpleado.value).subscribe(resp => {     
      
      this.respuestaAPI = resp;
      if (typeof(this.respuestaAPI.error) != 'undefined' && this.respuestaAPI.error) {
        this.errores = this.respuestaAPI.error;
      }
      else{
        this.router.navigate(['/empleado-index']);
      }     
    },
    error => { console.error(error) }    
    );
   
    
  }

}
