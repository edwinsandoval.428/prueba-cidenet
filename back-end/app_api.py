from flask import Flask, request
from flaskext.mysql import MySQL
import json
from flask.json import jsonify
from datetime import datetime
from flask_cors import CORS


app = Flask(__name__)
CORS(app)

mysql = MySQL()
app.config['MYSQL_DATABASE_HOST'] = 'localhost'
app.config['MYSQL_DATABASE_USER'] = 'root'
app.config['MYSQL_DATABASE_PASSWORD'] = 'Admin.1234'
app.config['MYSQL_DATABASE_DB'] = 'pruebas'

mysql.init_app(app)


@app.route('/api/getEmpleados')
def getEmpleados():

    try:
        sql = """SELECT e.id_empleado, 
                        e.primer_nombre,
                        e.otros_nombres,                      
                        e.primer_apellido,
                        e.segundo_apellido,
                        e.id_tipo_identificacion,
                        e.identificacion,
                        e.correo_electronico,
                        e.estado,
                        p.nombre as pais,
                        tp.nombre as tipo_identificacion,
                        a.nombre as area
                FROM pruebas.empleados e, pruebas.paises p, pruebas.tipos_documentos tp, pruebas.areas a
                WHERE e.id_pais = p.id_pais
                and e.id_tipo_identificacion = tp.id_tipo_documento
                and e.id_area = a.id_area
                order by e.fecha_registro desc; """
        conn = mysql.connect()

        cursor = conn.cursor()
        cursor.execute(sql)
        r = [dict((cursor.description[i][0], value) for i, value in enumerate(row)) for row in cursor.fetchall()]
        json_results = json.dumps( r )

        conn.commit()           

        response = app.response_class(
            response=json_results,
            status=200,
            mimetype='application/json'
        )
    except Exception as e: 
        response = app.response_class(
                response= json.dumps({"mensaje": "Error "+str(e)}),
                status=500,
                mimetype='application/json'
            )
    finally:        
        if 'cursor' in locals():    
            cursor.connection.close()     

    return response

@app.route('/api/getEmpleado/<int:id>')
def getEmpleado(id):

    try:

        sql = """SELECT id_empleado, 
                        primer_nombre,
                        otros_nombres,                      
                        primer_apellido,
                        segundo_apellido,
                        id_tipo_identificacion,
                        identificacion,
                        id_pais,
                        fecha_ingreso,
                        id_area 
                    FROM pruebas.empleados WHERE id_empleado = %s """
        conn = mysql.connect()

        cursor = conn.cursor()
        cursor.execute(sql,id)
        r = [dict((cursor.description[i][0], value) for i, value in enumerate(row)) for row in cursor.fetchall()]
        json_results = json.dumps( r,default=str )
        conn.commit()     

        response = app.response_class(
            response= json_results,
            status=200,
            mimetype='application/json'
        )
    except Exception as e: 
        response = app.response_class(
                response= json.dumps({"mensaje": "Error "+str(e)}),
                status=500,
                mimetype='application/json'
            )
    finally:        
        if 'cursor' in locals():    
            cursor.connection.close() 

    return response

@app.route('/api/getPaises')
def getPaises():

    try:
        sql = """SELECT id_pais, nombre, dominio  FROM pruebas.paises; """
        conn = mysql.connect()

        cursor = conn.cursor()
        cursor.execute(sql)
        r = [dict((cursor.description[i][0], value) for i, value in enumerate(row)) for row in cursor.fetchall()]
        json_results = json.dumps( r )

        conn.commit()

        response = app.response_class(
            response=json_results,
            status=200,
            mimetype='application/json'
        )
    except Exception as e: 
        response = app.response_class(
                response= json.dumps({"mensaje": "Error "+str(e)}),
                status=500,
                mimetype='application/json'
            )
    finally:        
        if 'cursor' in locals():    
            cursor.connection.close() 

    return response

@app.route('/api/getAreas')
def getAreas():

    try:
        sql = """SELECT id_area, nombre  FROM pruebas.areas; """
        conn = mysql.connect()

        cursor = conn.cursor()
        cursor.execute(sql)
        r = [dict((cursor.description[i][0], value) for i, value in enumerate(row)) for row in cursor.fetchall()]
        json_results = json.dumps( r )

        conn.commit()       

        response = app.response_class(
            response=json_results,
            status=200,
            mimetype='application/json'
        )

    except Exception as e: 
        response = app.response_class(
                response= json.dumps({"mensaje": "Error "+str(e)}),
                status=500,
                mimetype='application/json'
            )
    finally:        
        if 'cursor' in locals():    
            cursor.connection.close() 

    return response

@app.route('/api/getTiposDocumentos')
def getTiposDocumentos():

    try:
        sql = """SELECT id_tipo_documento, nombre, abreviacion FROM pruebas.tipos_documentos; """
        conn = mysql.connect()

        cursor = conn.cursor()
        cursor.execute(sql)
        r = [dict((cursor.description[i][0], value) for i, value in enumerate(row)) for row in cursor.fetchall()]
        json_results = json.dumps( r )

        conn.commit()   

        response = app.response_class(
            response=json_results,
            status=200,
            mimetype='application/json'
        )
    except Exception as e: 
        response = app.response_class(
                response= json.dumps({"mensaje": "Error "+str(e)}),
                status=500,
                mimetype='application/json'
            )
    finally:        
        if 'cursor' in locals():    
            cursor.connection.close()  

    return response

def contruirCorreo(primer_nombre,primer_apellido, id_pais):

    try: 

        sql = """SELECT dominio  FROM pruebas.paises where id_pais = %s; """      
               
        datos = (
            id_pais
        )
                
        print(sql)        
        conn = mysql.connect()
        cursor = conn.cursor()
        cursor.execute(sql,datos)                       

        print(cursor)
        rows = cursor.fetchall()
        dominio = rows[0][0]
        conn.commit() 
        cursor.connection.close() 
        correo = validarCorreo(primer_nombre+primer_apellido+"@cidenet.com."+dominio)

        return correo
    except Exception as e: 
        print (str(e))
        return "Error durante la ejecucion validarCorreo "+str(e)

def validarCorreo(correo):

    try: 
        correo = correo.lower()
        rows = 1
        repetidos = 0
        sql = """SELECT correo_electronico  FROM pruebas.empleados where lower(correo_electronico) = lower(%s); """
        
        while rows > 0:    
            if repetidos > 0:

                correo_array = correo.split('@')
                datos = (
                    correo_array[0]+str(repetidos)+"@"+correo_array[1]
                )
            else:
                datos = (
                    correo
                )
        
            conn = mysql.connect()
            cursor = conn.cursor()
            cursor.execute(sql,datos)      
            conn.commit()            

            rows = cursor.rowcount
            repetidos += 1

            cursor.connection.close() 

        return datos

    except Exception as e: 
        print(str(e))
        return "Error durante la ejecucion validarCorreo "+correo+str(e)




def validarIdentificacion(tipoIdentificacion, identificacion, id_empleado):

    try: 
        sql = """SELECT identificacion  FROM pruebas.empleados where id_tipo_identificacion = %s AND lower(identificacion) = lower(%s) and id_empleado != %s; """
        
        datos = (
                    tipoIdentificacion,
                    identificacion.lower(),
                    id_empleado
                )
               
        conn = mysql.connect()
        cursor = conn.cursor()
        cursor.execute(sql,datos)      
        conn.commit()            

        rows = cursor.rowcount
        cursor.connection.close() 

        return rows

    except Exception as e: 
        return "Error durante la ejecucion validarIdentificacion "+tipoIdentificacion +" - "+ identificacion +str(e)

 

@app.route('/api/crearEmpleado', methods=['POST'])
def crearEmpleado():

    try:
        json_empleado = request.get_json()

        #Validaciones
        #Validar numero identificacion
        if(validarIdentificacion(json_empleado['id_tipo_identificacion'], json_empleado['identificacion'],0 )>0):
            return app.response_class(
            response=json.dumps({"error": "Tipo y numero de identificacion ya existen"}),
            status=200,
            mimetype='application/json')


        correo = json_empleado['primer_nombre']+json_empleado['primer_apellido']+'@cidenet.com'
        print(correo)

        datos = (
            json_empleado['primer_nombre'],
            json_empleado['primer_apellido'],
            json_empleado['segundo_apellido'],
            json_empleado['otros_nombres'],         
            json_empleado['id_tipo_identificacion'],
            json_empleado['identificacion'],         
            json_empleado['id_pais'],
            contruirCorreo(json_empleado['primer_nombre'], json_empleado['primer_apellido'], json_empleado['id_pais']),
            json_empleado['fecha_ingreso'],
            json_empleado['id_area'],
            datetime.today(),
            datetime.today()
        )
        
        sql = """INSERT INTO `pruebas`.`empleados`(
                    `primer_nombre`,
                    `primer_apellido`,
                    `segundo_apellido`,
                    `otros_nombres`,
                    `id_tipo_identificacion`,
                    `identificacion`,
                    `id_pais`,
                    `correo_electronico`,
                    `fecha_ingreso`,
                    `id_area`,
                    `fecha_registro`,
                    `fecha_edicion`)
                VALUES
                    (%s,
                    %s,
                    %s,
                    %s,
                    %s,
                    %s,
                    %s,
                    %s,
                    %s,
                    %s,
                    %s,
                    %s);
                    """

        conn = mysql.connect()
        cursor = conn.cursor()
        cursor.execute(sql,datos)
        conn.commit()        

        response = app.response_class(
            response=json.dumps({"mensaje": "Empleado Creado"}),
            status=200,
            mimetype='application/json'
        )
        
    except Exception as e: 
        response = app.response_class(
                response= json.dumps({"mensaje": "Error "+str(e)}),
                status=500,
                mimetype='application/json'
            )
    finally:        
        if 'cursor' in locals():    
            cursor.connection.close()  

    return response


@app.route('/api/actualizarEmpleado/<int:id>', methods=['POST'])
def actualizarEmpleado(id):

    try:

        json_empleado = request.get_json()

        #Validaciones
        #Validar numero identificacion
        if(validarIdentificacion(json_empleado['id_tipo_identificacion'], json_empleado['identificacion'], id)>0):
            return app.response_class(
            response=json.dumps({"error": "Tipo y numero de identificacion ya existen"}),
            status=200,
            mimetype='application/json')
       

        datos = (
            json_empleado['primer_nombre'],
            json_empleado['primer_apellido'],
            json_empleado['segundo_apellido'],
            json_empleado['otros_nombres'],
            json_empleado['id_pais'],
            contruirCorreo(json_empleado['primer_nombre'], json_empleado['primer_apellido'], json_empleado['id_pais']),
            json_empleado['id_tipo_identificacion'],
            json_empleado['identificacion'],
            json_empleado['id_area'],
            json_empleado['fecha_ingreso'],
            datetime.today(),
            id

        )

        sql = """UPDATE `pruebas`.`empleados`
                SET
                `primer_nombre` = %s,
                `primer_apellido` = %s,
                `segundo_apellido` = %s,
                `otros_nombres` = %s,            
                `id_pais` = %s,
                `correo_electronico` = %s,
                `id_tipo_identificacion` = %s,
                `identificacion` = %s, 
                `id_area` = %s,
                `fecha_ingreso` = %s,
                `fecha_edicion` = %s
                WHERE `id_empleado` = %s; """

        conn = mysql.connect()

        cursor = conn.cursor()
        cursor.execute(sql,datos)
        conn.commit()

        response = app.response_class(
            response=json.dumps({"mensaje": "Empleado actualizado"}),
            status=200,
            mimetype='application/json'
        )

    except Exception as e: 
        response = app.response_class(
                response= json.dumps({"mensaje": "Error "+str(e)}),
                status=500,
                mimetype='application/json'
            )
    finally:                
        if 'cursor' in locals():    
            cursor.connection.close() 

    return response
    



@app.route('/api/eliminarEmpleado/<int:id>')
def eliminarEmpleado(id):

    try:
        sql = "DELETE FROM pruebas.empleados WHERE id_empleado = %s"
        conn = mysql.connect()

        cursor = conn.cursor()
        cursor.execute(sql,id)
        conn.commit()     

        response = app.response_class(
            response= json.dumps({"mensaje": "Empleado Eliminado"}),
            status=200,
            mimetype='application/json'
        )

    except Exception as e: 
        response = app.response_class(
                response= json.dumps({"mensaje": "Error "+str(e)}),
                status=500,
                mimetype='application/json'
            )
    finally:   
        if 'cursor' in locals():    
            cursor.connection.close() 

    return response




if __name__ == '__main__':

    app.run(debug=True)